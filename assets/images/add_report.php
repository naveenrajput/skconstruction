<style type="text/css">
  input[type="file"] {
  display: block;

}
#files {
  display:none;
}

#file-link {
   @include button;
   margin:30px 0 0 40px;
 }

.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>

<form role="form bor-rad" enctype="multipart/form-data" action="<?php echo base_url().'user/add_Report'?>" method="post">
  <div class="box-body">
    <div class="row">
          
						<div class="col-md-12">
				          <div class="form-group form-float">
				            <div class="form-line">
                      <?php $project = $this->db->get_where('project',array('status'=>'active'))->result();?>
				            	<select name="project" id="" class="form-control" required >
                        <option >Select</option>
                        <?php foreach ($project as $key) { ?>
                          <option value="<?php echo $key->pname;?>"><?php echo $key->pname;?></option>
                        <?php }?>
        			        			
        			        </select>
				            	<label class="form-label" for="status"> <?php echo lang('project') ?></label>
				            </div>
				          </div>
				        </div>
					
						
					
						<div class="col-md-5">
						  <div class="form-group form-float">
						  	<div class="form-line">
							    <input type="text" name="expenditure[]" value="" required class="form-control">
							    <label class="form-label"><?php echo lang('expenditure') ?></label>
						    </div>
						  </div>
						</div>
					
          <div class="col-md-5">
          <div class="form-group form-float">
           
            <div class="form-line">
              <input type="text" name="name[]" value="" required class="form-control">
                <label for="" class="form-label"><?php echo lang('name'); ?></label>
               
              
            </div>
          </div> 
        </div>
         <div class="col-md-2">
          
            <button class="btn btn-info ad-u-type-btn btn-sm" type="button" title="<?php echo lang('add_new'); ?>"><i class="material-icons">add_circle</i></button>
         
          </div>
          <div id='custume'></div>
        <div class="col-md-12">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="text" style="display: none">

              <textarea type="test" name="comment" class="form-control" value=""></textarea>
              <label for="" class="form-label"><?php echo lang('comment'); ?></label>
            </div>
          </div>
        </div>
       <div class="col-md-12"> 
         <a href="#" class="btn btn-info" id="file-link"><?php echo lang('image_upload'); ?></a>
            <div class="form-group imsize">
          
               <input type="file" id="files" name="files[]" multiple />
            </div>
          </div>                
        </div>
        <?php get_custom_fields('report', isset($userData->users_id)?$userData->users_id:NULL); ?>   
        <?php if(!empty($userData->users_id)){?>
        <input type="hidden"  name="users_id" value="<?php echo isset($userData->users_id)?$userData->users_id:'';?>">
        <input type="hidden" name="fileOld" value="<?php echo isset($userData->profile_pic)?$userData->profile_pic:'';?>">
        <div class="box-footer sub-btn-wdt">
          <button type="submit" name="edit" value="edit" class="btn btn-primary wdt-bg"><?php echo lang('Update'); ?></button>
        </div>
              <!-- /.box-body -->
        <?php }else{?>
        <div class="box-footer sub-btn-wdt">
          <button type="submit" name="submit" value="add" class="btn btn-primary wdt-bg"><?php echo lang('Add'); ?></button>
        </div>
        <?php }?>
      </form>
<script>
  $.AdminBSB.input.activate();
</script>
<script type="text/javascript">
  $('#file-link').click(function() {
  $('#files').click();
});
    $(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove Attachment</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
$(document).ready(function() {
  $(".delete").hide();
  //when the Add Field button is clicked
  $(".btn-sm").click(function(e) {
    $(".delete").fadeIn("1500");
    //Append a new row of code to the "#items" div
    $("#custume").append(
      '<div class="next-referral"><div class="col-md-5"><div class="form-group form-float"><div class="form-line"><input type="text" name="expenditure[]" value="" required class="form-control" placeholder="<?php echo lang('expenditure') ?>"></div></div></div><div class="col-md-5"><div class="form-group form-float"><div class="form-line"><input type="text" name="name[]" value="" required class="form-control" placeholder="<?php echo lang('name') ?>"></div></div></div><div class="col-md-2"><button class="btn btn-danger rm-u-type-btn delete" type="button" title="<?php echo lang('remove'); ?>"><i class="material-icons">remove_circle</i></button></div>'
    );
  });
  $("body").on("click", ".delete", function(e) {
    $(".next-referral").last().remove();
  });
});


</script>