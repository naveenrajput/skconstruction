
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>
               <?php echo lang("report");?>                                
            </h2>
            <ul class="header-dropdown m-r--5">
                
                  <button type="button" class="btn btn-lg btn-primary waves-effect modalButtonReportA" data-toggle="modal"><i class="material-icons">add</i> <?php echo lang('report'); ?></button>
                <?php  if(setting_all('email_invitation') == 1){  ?>
                  <button type="button" class="btn btn-lg btn-primary waves-effect InviteUser" data-toggle="modal"><i class="material-icons">add</i><?php echo lang('invite_people'); ?></button>
                <?php } ?>
            </ul>
          </div>
          <!-- /.box-header -->
          <div class="body table-responsive">           
            <table id="example12" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>
                  <th>
                    <input type="checkbox" class="selAll" id="basic_checkbox_1mka" />
                    <label for="basic_checkbox_1mka"></label>
                  </th>
                  <th><?php echo lang('project') ?></th>
                  <th><?php echo lang('name') ?></th>
                  <th><?php echo lang('total') ?></th>
                  <th><?php echo lang('create_date') ?></th>
                    
                  <?php  $cf = get_cf('report');
                    if(is_array($cf) && !empty($cf)) {
                      foreach ($cf as $cfkey => $cfvalue) {
                        echo '<th>'.lang(get_lang($cfvalue->rel_crud).'_'.get_lang($cfvalue->name)).'</th>';
                      }
                    }
                    
                  ?>
                  <th><?php echo lang('report'); ?></th>
                   <th><?php echo lang('Attechment'); ?></th>
                 

                 

                </tr>
              </thead>
              <tbody>
              </tbody> 
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>

<div class="modal fade" id="nameModal_report" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="defaultModalLabel"><?php echo lang('report_form'); ?></h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div><!--End Modal Crud --> 
<script type="text/javascript">
  $(document).ready(function() {  
    var url = '<?php echo base_url();?>';
    var table = $('#example12').DataTable({ 
          dom: 'lfBrtip',
          buttons: [
              'copy', 'excel', 'pdf', 'print'
          ],
          "processing": true,
          "serverSide": true,
          "ajax": url+"admin/reportTable",
          "sPaginationType": "full_numbers",
          "language": {
            "search": "_INPUT_", 
            "searchPlaceholder": "<?php echo lang('search'); ?>",
            "paginate": {
                "next": '<i class="material-icons">keyboard_arrow_right</i>',
                "previous": '<i class="material-icons">keyboard_arrow_left</i>',
                "first": '<i class="material-icons">first_page</i>',
                "last": '<i class="material-icons">last_page</i>'
            }
          }, 
          "iDisplayLength": 10,
          "aLengthMenu": [[10, 25, 50, 100,500,-1], [10, 25, 50,100,500,"<?php echo lang('all'); ?>"]],
          "aoColumnDefs": [ 
            { "bSortable": false, "aTargets": [ 0 ] }
          ]
      });
    
    setTimeout(function() {
      var add_width = $('.dataTables_filter').width()+$('.box-body .dt-buttons').width()+10;
      $('.table-date-range').css('right',add_width+'px');

           
    }, 300);
    $("button.closeTest, button.close").on("click", function (){});
  });
 var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption

var captionText = document.getElementById("caption");
 function myfun(obj,abc){
  
 var cl = $(obj).attr('class');
 var img = $('.'+cl).attr("src");

    var modalImg = "<img src='"+img+"' style='width: 500px;height:500px'>";

  $('#nameModal_report').find('.modal-body').html(modalImg);
      $('#nameModal_report').modal('show'); 
  
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>            