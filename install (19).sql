-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 05:14 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `install`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_token`
--

CREATE TABLE `api_token` (
  `id` int(11) UNSIGNED NOT NULL,
  `token` varchar(300) DEFAULT NULL,
  `activity_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cf_values`
--

CREATE TABLE `cf_values` (
  `cf_values_id` int(11) UNSIGNED NOT NULL,
  `rel_crud_id` int(11) DEFAULT NULL,
  `cf_id` int(11) DEFAULT NULL,
  `curd` varchar(250) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cf_values`
--

INSERT INTO `cf_values` (`cf_values_id`, `rel_crud_id`, `cf_id`, `curd`, `value`) VALUES
(3, 1, 15, 'user', 'fff'),
(4, 15, 4, 'report', '333'),
(5, 1, 6, 'report', 'xcxc'),
(6, 1, 8, 'report', 'b'),
(7, 1, 7, 'project', 'tttt'),
(9, 1, 1, 'report', 'xcxcxc'),
(10, 1, 2, 'project', 'gggggggg'),
(11, 16, 1, 'report', 'xcxc'),
(12, 17, 1, 'report', 'cccc'),
(13, 18, 1, 'report', 'ddddddddddd'),
(14, 2, 2, 'project', 'xxxx'),
(15, 2, 2, 'project', 'xxxxxxxxxxxxxxx');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `custom_fields_id` int(11) UNSIGNED NOT NULL,
  `rel_crud` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `options` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `show_in_grid` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`custom_fields_id`, `rel_crud`, `name`, `type`, `required`, `options`, `status`, `show_in_grid`, `create_date`) VALUES
(1, 'report', 'rtt', 'text', 1, NULL, 'active', 1, '2020-07-08 15:14:31'),
(2, 'project', 'type', 'text', 1, NULL, 'active', 1, '2020-07-09 06:08:07');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `expenses_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `upload_receipt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expense_category`
--

CREATE TABLE `expense_category` (
  `expense_category_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `income_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `income_category`
--

CREATE TABLE `income_category` (
  `income_category_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(122) UNSIGNED NOT NULL,
  `user_type` varchar(250) DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `user_type`, `data`) VALUES
(1, 'admin', '{\"user\":\"user\"}'),
(2, 'user', '{\"user\":{\"own_read\":\"1\",\"all_read\":\"1\"}}'),
(3, 'admin', '{\"user\":\"user\"}'),
(4, 'admin', '{\"user\":\"user\"}');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(225) NOT NULL,
  `pname` varchar(225) DEFAULT NULL,
  `pdepartment` varchar(225) DEFAULT NULL,
  `pprice` varchar(225) DEFAULT NULL,
  `pexpenditure` varchar(225) DEFAULT NULL,
  `pincome` varchar(225) DEFAULT NULL,
  `startdate` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `comments` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `pname`, `pdepartment`, `pprice`, `pexpenditure`, `pincome`, `startdate`, `status`, `comments`) VALUES
(8, 'ggggggggg', 'ggggggggg', '25000', NULL, NULL, '', 'active', 'gggggggg'),
(9, 'aaaaaa', 'aaaaaaaaaa', '25000', NULL, NULL, '', 'active', 'aaaaaaaaaa'),
(10, 'nkt@gmail.com', 'ddfd', '25000', NULL, NULL, '', 'Complete', 'dfdf'),
(11, 'nkt@gmail.com', 'ddfd', '25000', NULL, NULL, '', 'Complete', 'dfdf');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(225) NOT NULL,
  `project` varchar(225) DEFAULT NULL,
  `user_id` int(225) DEFAULT NULL,
  `expenditure` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `total` int(225) DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `attechment` varchar(225) DEFAULT '123',
  `createdate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id`, `project`, `user_id`, `expenditure`, `name`, `total`, `comment`, `attechment`, `createdate`) VALUES
(6, 'pipeline', 1, '200', 'lebour', 200, 'xcxc', 'PHOTO4.jpg,qr-code2.png', '2020-07-08 17:03:27'),
(7, 'pipeline', 1, '200,200', 'tagari,lebour', 400, 'xcxcx', 'noattechment.jpg', '2020-07-08 17:14:10'),
(8, 'pipeline', 1, '200', 'tagari', 200, 'ccvv', 'noattechment.jpg', '2020-07-08 17:16:27'),
(9, 'pipeline', 1, '200', 'tagari', 200, 'vcv', 'noattechment.jpg', '2020-07-08 17:22:49'),
(11, 'pipeline', 1, 'xx', 'tagari', 0, 'xcxc', 'noattechment.jpg', '2020-07-08 17:26:18'),
(13, 'water Tank', 1, '200', 'tagari', 200, 'xxz', 'noattechment.jpg', '2020-07-08 20:06:02'),
(14, 'water Tank', 1, '200', 'tagari', 200, 'cc', 'noattechment.jpg', '2020-07-08 20:07:57'),
(15, 'water Tank', 1, '200', 'tagari', 200, 'xxx', 'noattechment.jpg', '2020-07-08 20:45:19'),
(16, 'water Tank', 1, '200', 'tagari', 200, 'dvdvd', 'PHOTO5.jpg', '2020-07-09 12:04:22'),
(17, 'water Tank', 1, '200', 'lebour', 200, 'cccc', 'noattechment.jpg', '2020-07-09 15:05:04'),
(18, 'water Tank', 1, '200', 'tagari', 200, 'dddddddddddddddd', 'noattechment.jpg', '2020-07-09 15:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(122) UNSIGNED NOT NULL,
  `keys` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `keys`, `value`) VALUES
(1, 'website', 'sk.construction'),
(2, 'logo', 'background_old_1592820134.jpg'),
(3, 'favicon', 'search_1592820157.png'),
(4, 'SMTP_EMAIL', ''),
(5, 'HOST', ''),
(6, 'PORT', ''),
(7, 'SMTP_SECURE', ''),
(8, 'SMTP_PASSWORD', ''),
(9, 'mail_setting', 'simple_mail'),
(10, 'company_name', 'Company Name'),
(11, 'crud_list', 'User,Report,Project'),
(12, 'EMAIL', ''),
(13, 'UserModules', 'yes'),
(14, 'register_allowed', '1'),
(15, 'email_invitation', '1'),
(16, 'admin_approval', '1'),
(17, 'language', 'english'),
(18, 'user_type', '[\"user\"]'),
(19, 'date_formate', ''),
(20, 'api_status', 'disabled'),
(21, 'api_key', ''),
(22, 'token_expiration', '20');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(121) UNSIGNED NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `html` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `module`, `code`, `template_name`, `html`) VALUES
(1, 'forgot_pass', 'forgot_password', 'Forgot password', '<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n\n  <style type=\"text/css\" rel=\"stylesheet\" media=\"all\">\n\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n\n      font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\n\n      -webkit-box-sizing: border-box;\n\n      box-sizing: border-box;\n\n    }\n\n    body {\n\n      \n\n    }\n\n    a {\n\n      color: #3869D4;\n\n    }\n\n\n\n\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n\n      padding: 25px 0;\n\n      text-align: center;\n\n    }\n\n    .email-masthead_logo {\n\n      max-width: 400px;\n\n      border: 0;\n\n    }\n\n    .email-footer {\n\n      width: 570px;\n\n      margin: 0 auto;\n\n      padding: 0;\n\n      text-align: center;\n\n    }\n\n    .email-footer p {\n\n      color: #AEAEAE;\n\n    }\n\n  \n\n    .content-cell {\n\n      padding: 35px;\n\n    }\n\n    .align-right {\n\n      text-align: right;\n\n    }\n\n\n\n    /* Type ------------------------------ */\n\n    h1 {\n\n      margin-top: 0;\n\n      color: #2F3133;\n\n      font-size: 19px;\n\n      font-weight: bold;\n\n      text-align: left;\n\n    }\n\n    h2 {\n\n      margin-top: 0;\n\n      color: #2F3133;\n\n      font-size: 16px;\n\n      font-weight: bold;\n\n      text-align: left;\n\n    }\n\n    h3 {\n\n      margin-top: 0;\n\n      color: #2F3133;\n\n      font-size: 14px;\n\n      font-weight: bold;\n\n      text-align: left;\n\n    }\n\n    p {\n\n      margin-top: 0;\n\n      color: #74787E;\n\n      font-size: 16px;\n\n      line-height: 1.5em;\n\n      text-align: left;\n\n    }\n\n    p.sub {\n\n      font-size: 12px;\n\n    }\n\n    p.center {\n\n      text-align: center;\n\n    }\n\n\n\n    /* Buttons ------------------------------ */\n\n    .button {\n\n      display: inline-block;\n\n      width: 200px;\n\n      background-color: #3869D4;\n\n      border-radius: 3px;\n\n      color: #ffffff;\n\n      font-size: 15px;\n\n      line-height: 45px;\n\n      text-align: center;\n\n      text-decoration: none;\n\n      -webkit-text-size-adjust: none;\n\n      mso-hide: all;\n\n    }\n\n    .button--green {\n\n      background-color: #22BC66;\n\n    }\n\n    .button--red {\n\n      background-color: #dc4d2f;\n\n    }\n\n    .button--blue {\n\n      background-color: #3869D4;\n\n    }\n\n  </style>\n\n</head>\n\n<body style=\"width: 100% !important;\n\n      height: 100%;\n\n      margin: 0;\n\n      line-height: 1.4;\n\n      background-color: #F2F4F6;\n\n      color: #74787E;\n\n      -webkit-text-size-adjust: none;\">\n\n  <table class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\n\n    width: 100%;\n\n    margin: 0;\n\n    padding: 0;\">\n\n    <tbody><tr>\n\n      <td align=\"center\">\n\n        <table class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\n\n      margin: 0;\n\n      padding: 0;\">\n\n          <!-- Logo -->\n\n\n\n          <tbody>\n\n          <!-- Email Body -->\n\n          <tr>\n\n            <td class=\"email-body\" width=\"100%\" style=\"width: 100%;\n\n    margin: 0;\n\n    padding: 0;\n\n    border-top: 1px solid #edeef2;\n\n    border-bottom: 1px solid #edeef2;\n\n    background-color: #edeef2;\">\n\n              <table class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\" style=\" width: 570px;\n\n    margin:  14px auto;\n\n    background: #fff;\n\n    padding: 0;\n\n    border: 1px outset rgba(136, 131, 131, 0.26);\n\n    box-shadow: 0px 6px 38px rgb(0, 0, 0);\n\n       \">\n\n                <!-- Body content -->\n\n                <thead style=\"background: #3869d4;\"><tr><th><div align=\"center\" style=\"padding: 15px; color: #000;\"><a href=\"{var_action_url}\" class=\"email-masthead_name\" style=\"font-size: 16px;\n\n      font-weight: bold;\n\n      color: #bbbfc3;\n\n      text-decoration: none;\n\n      text-shadow: 0 1px 0 white;\">{var_sender_name}</a></div></th></tr>\n\n                </thead>\n\n                <tbody><tr>\n\n                  <td class=\"content-cell\" style=\"padding: 35px;\">\n\n                    <h1>Hi {var_user_name},</h1>\n\n                    <p>You recently requested to reset your password for your {var_website_name} account. Click the button below to reset it.</p>\n\n                    <!-- Action -->\n\n                    <table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\n\n      width: 100%;\n\n      margin: 30px auto;\n\n      padding: 0;\n\n      text-align: center;\">\n\n                      <tbody><tr>\n\n                        <td align=\"center\">\n\n                          <div>\n\n                            <!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"{{var_action_url}}\" style=\"height:45px;v-text-anchor:middle;width:200px;\" arcsize=\"7%\" stroke=\"f\" fill=\"t\">\n\n                              <v:fill type=\"tile\" color=\"#dc4d2f\" />\n\n                              <w:anchorlock/>\n\n                              <center style=\"color:#ffffff;font-family:sans-serif;font-size:15px;\">Reset your password</center>\n\n                            </v:roundrect><![endif]-->\n\n                            <a href=\"{var_varification_link}\" class=\"button button--red\" style=\"background-color: #dc4d2f;display: inline-block;\n\n      width: 200px;\n\n      background-color: #3869D4;\n\n      border-radius: 3px;\n\n      color: #ffffff;\n\n      font-size: 15px;\n\n      line-height: 45px;\n\n      text-align: center;\n\n      text-decoration: none;\n\n      -webkit-text-size-adjust: none;\n\n      mso-hide: all;\">Reset your password</a>\n\n                          </div>\n\n                        </td>\n\n                      </tr>\n\n                    </tbody></table>\n\n                    <p>If you did not request a password reset, please ignore this email or reply to let us know.</p>\n\n                    <p>Thanks,<br>{var_sender_name} and the {var_website_name} Team</p>\n\n                   <!-- Sub copy -->\n\n                    <table class=\"body-sub\" style=\"margin-top: 25px;\n\n      padding-top: 25px;\n\n      border-top: 1px solid #EDEFF2;\">\n\n                      <tbody><tr>\n\n                        <td> \n\n                          <p class=\"sub\" style=\"font-size:12px;\">If you are having trouble clicking the password reset button, copy and paste the URL below into your web browser.</p>\n\n                          <p class=\"sub\"  style=\"font-size:12px;\"><a href=\"{var_varification_link}\">{var_varification_link}</a></p>\n\n                        </td>\n\n                      </tr>\n\n                    </tbody></table>\n\n                  </td>\n\n                </tr>\n\n              </tbody></table>\n\n            </td>\n\n          </tr>\n\n        </tbody></table>\n\n      </td>\n\n    </tr>\n\n  </tbody></table>\n\n\n\n\n\n</body></html>'),
(2, 'users', 'invitation', 'Invitation', '<p>Hello <strong>{var_user_email}</strong></p>\n\n\n\n<p>Click below link to register&nbsp;<br />\n\n{var_inviation_link}</p>\n\n\n\n<p>Thanks&nbsp;</p>\n\n'),
(3, 'registration', 'registration', 'Registration', '<p>Hello <strong>{var_user_name}</strong></p>\n\n<p>Welcome to Notes&nbsp;<br />\n\n<p>To complete your registration&nbsp;<br /><br />\n\n<a href=\"{var_varification_link}\">please click here</a></p>\n\n\n<p>Thanks&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `var_key` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `is_deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `user_id`, `var_key`, `status`, `is_deleted`, `name`, `password`, `email`, `profile_pic`, `user_type`, `create_date`) VALUES
(2, '1', NULL, 'active', '0', 'Naveen Singh Rajput', '$2y$10$pCyChOwKt0v2vRTd2.hvFeupzdz4ALn98Ri2b8bbQfOmsVdyPG4.G', 'naveen@gmail.com', 'stripe-payment-logo_1592818886.png', 'admin', '2020-06-22'),
(3, '1', NULL, 'active', '0', 'dinesh', '$2y$10$qrLaqImg.C8t6X2nOcxtW.Q0r0awsC8I/Wm9zZpcAUSWcane3tbFS', 'dinesh@gmail.com', 'Screenshot (7)_1592896881.png', 'user', '2020-06-23'),
(5, '1', NULL, 'active', '0', 'Naveen Singh Rajput', '$2y$10$RColNBzSdcLIOYAoSZIHhOrXdxK0/VP0TPXpmmKpUM/1gstnNBW8e', 'root@gmail.com', 'PHOTO_1594195416.jpg', 'user', '2020-07-08'),
(6, '1', NULL, 'active', '0', 'cdf', '$2y$10$f4jkTLbjeiKGptoF66zaWeao/puecGlgNOg8C3mwBQaNG9qSyYM1O', 'admin@dispostable.com', 'PHOTO_1594196408.jpg', 'user', '2020-07-08');

-- --------------------------------------------------------

--
-- Table structure for table `wpb_c_modules`
--

CREATE TABLE `wpb_c_modules` (
  `id` int(122) UNSIGNED NOT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wpb_extension`
--

CREATE TABLE `wpb_extension` (
  `extension_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `db_tables` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `inst_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `rm_queries` longtext DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cf_values`
--
ALTER TABLE `cf_values`
  ADD PRIMARY KEY (`cf_values_id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`custom_fields_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`expenses_id`);

--
-- Indexes for table `expense_category`
--
ALTER TABLE `expense_category`
  ADD PRIMARY KEY (`expense_category_id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`income_id`);

--
-- Indexes for table `income_category`
--
ALTER TABLE `income_category`
  ADD PRIMARY KEY (`income_category_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- Indexes for table `wpb_c_modules`
--
ALTER TABLE `wpb_c_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wpb_extension`
--
ALTER TABLE `wpb_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_token`
--
ALTER TABLE `api_token`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cf_values`
--
ALTER TABLE `cf_values`
  MODIFY `cf_values_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `custom_fields_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `expenses_id` int(121) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense_category`
--
ALTER TABLE `expense_category`
  MODIFY `expense_category_id` int(121) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `income_id` int(121) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `income_category`
--
ALTER TABLE `income_category`
  MODIFY `income_category_id` int(121) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(121) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wpb_c_modules`
--
ALTER TABLE `wpb_c_modules`
  MODIFY `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wpb_extension`
--
ALTER TABLE `wpb_extension`
  MODIFY `extension_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
